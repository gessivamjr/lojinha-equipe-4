import React from 'react'
import EditProduct from '../../components/edit-product/EditProduct'

export default function Edit() {
    return (
        <div>
            <EditProduct />
        </div>
    )
}

