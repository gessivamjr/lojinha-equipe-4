import mapIcon from '/icons/ep_map-location.svg'
import './search-location.css'

export default function SearchLocation() {
    return (
        <div className='search-location-container'>
            <header className='location-header'>
                <img src={mapIcon} />
                <h1>Buscar CEP</h1>
            </header>
            <div className="search-input-container">
                <input type='text' placeholder='Digite seu CEP ' />
                <button>Aplicar</button>
            </div>
            <hr className='location-divisory' />

        </div>
    )
}