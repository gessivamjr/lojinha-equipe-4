import React from 'react';
import './footer.css';
import { Link } from 'react-router-dom';

export default function Footer() {
    return (
        <footer>
            <div className="footer">
                <div className="logo-container">
                    <div className="logo-footer">
                        <img src="images/Logo.svg" alt="ícone de uma sacola de compras verde" />
                        
                        <Link to="/">Lojinha</Link>
                    </div>
                </div>
                
                <div className="list-container">
                    <ol className="list-menu">
                        <li>
                            <Link to="/">Início</Link>
                        </li>

                        <li>
                            <a  href="#products"> Produtos</a>
                        </li>

                        <li>
                            <Link to="/announce">Anunciar</Link>
                        </li>

                        <li>
                            <Link to="/shop">Carrinho</Link>
                        </li>
                    </ol>
                </div>

                <div className="social-container">
                    <div className="social">
                        <a href="#"><i className="fab fa-facebook"></i></a>
                        <a href="#"><i className="fab fa-twitter"></i></a>
                        <a href="#"><i className="fab fa-instagram"></i></a>
                    </div>
                </div>  
            </div>

            <div className="copyright">
                <p>Feito pela <a href="https://infojr.com.br/" target="_blank">InfoJr</a> UFBA com Figma, React e muito <i className="fas fa-heart"></i></p>
            </div>
        </footer>
    )
}
