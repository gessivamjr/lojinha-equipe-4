import CartList from '../cart-list/CartList'
import SearchLocation from '../search-location/SearchLocation'
import arrowIcon from '/icons/dashicons_arrow-right.svg'
import './cart.css'
import CartInvoice from '../cart-invoice/CartInvoice'
import { useState } from 'react'
import { useEffect } from 'react'
import EmptyCart from '../empty-cart/EmptyCart'


export default function Cart() {

    const [subtotal, setSubtotal] = useState(0)
    const [emptyCart, setEmptyCart] = useState(false)

    if (emptyCart == true) {
        return (
            <EmptyCart />
        )
    }

    return (
        <div>

            <main className="cart-container">
                <div className="page-location">
                    <img src={arrowIcon} />
                    <h3>Carrinho</h3>
                </div>
                <div className="cart-content">
                    <div className="cart-first-column">
                        <div className="search-location">
                            <SearchLocation />
                        </div>

                        <div className="cart-products-container">
                            <CartList
                                setSubtotal={setSubtotal}
                            />
                        </div>
                    </div>

                    <div className="cart-second-column">
                        <CartInvoice subtotal={subtotal} />
                    </div>
                </div>
            </main>
        </div>
    )
}