import { useEffect } from 'react'
import { useState } from 'react'
import mockData from '../../utils/mockData'
import './product-tile.css'

export default function ProductTile({ product, deleteProduct }) {

    const [quantity, setQuantity] = useState(1)

    function changeQuantity(isAdding) {

        if (isAdding == true) {
            quantity == product.quantityInStock ? null : setQuantity(quantity + 1)
        }
        else {
            quantity == 0 ? null : setQuantity(quantity - 1)
        }
    }

    useEffect(() => {
        product.quantity = quantity
    }, [quantity])

    return (
        <tbody>
            <tr>
                <td>
                    <div className="product-tile-container">
                        <img src={product.imageUrl} alt={"imagem de " + product.description} />
                        <div className="product-tile-description-container">
                            <div className="description-first-column">
                                <div className="description-header">
                                    <h1>{product.description}</h1>
                                    <h4>Vendido por <span>{product.seller}</span></h4>
                                </div>
                                <div className="description-content">
                                    <p>Cor: {product.color}</p>
                                    <p>Tamanho: {product.size}</p>
                                    <p>Marca: {product.category}</p>
                                </div>
                            </div>
                            <div className="description-second-column">
                                <div className="tile-quantity-container">
                                    <button onClick={(e) => changeQuantity(false)}>-</button>
                                    <span>{quantity}</span>
                                    <button onClick={(e) => changeQuantity(true)}>+</button>
                                </div>
                                <p>R$ {product.price}</p>
                                <button className="tile-delete-button" onClick={(e) => deleteProduct(product.id)}>Deletar</button>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>

    )
}