import { useState } from 'react'
import SeenRecently from '../seen-recently/SeenRecently'
import Modal from '../modal/Modal'
import './empty-cart.css'
import arrowIcon from '/icons/dashicons_arrow-right.svg'

export default function EmptyCart() {

    const [showModal, setShowModal] = useState(false)
    const [id, setId] = useState(1)

    function handleClick(event) {
        setId(id)
        showModal == false ? setShowModal(true) : setShowModal(false)
    }

    return (
        <main className='empty-cart-container'>
            <div className="empty-cart-content">
                <div className="empty-cart-first-column">
                    <div className="page-location">
                        <img src={arrowIcon} />
                        <h3>Carrinho</h3>
                    </div>
                    <div className="empty-cart-message-container">
                        <div className="empty-cart-message">
                            <h2>Seu carrinho está vazio :(</h2>
                            <h4><a href='#'>Continue navegando</a> pela Lojinha e encontre produtos incríveis!</h4>
                        </div>
                    </div>
                </div>
                <div className="seen-recently">
                    <SeenRecently modalFunction={handleClick} />
                    <Modal isShowing={showModal} setIsShowing={setShowModal} id={id} />
                </div>
            </div>
        </main>
    )
}