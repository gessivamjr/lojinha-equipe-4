import { useState, useEffect } from 'react'
import './pagination-component.css'
import leftArrow from '/icons/bi_arrow-left-square-fill.svg'
import rightArrow from '/icons/bi_arrow-right-square-fill.svg'

export default function PaginationComponent({ setCurrentPage, pages, currentPage, scrollTo }) {

    const [pagesToShow, setPagesToShow] = useState([])
    let array = [];
    let next = 0;

    function setArrayOfPages() {
        let setOfPages = [];

        Array.from(Array(pages), (item, index) => {

            if (index == currentPage || index == currentPage + 1 || index == currentPage + 2 || index == pages - 1) {
                setOfPages.push(index)
            }
            else {

                // adds "..." after 3 first numbers shown
                (index == currentPage + 3) ? setOfPages.push("...") : null;

                //adds "..." before the last pages
                (currentPage >= pages - 4 && setOfPages.indexOf("...") == -1) ? setOfPages.push("...") : null
            }
        })

        return setOfPages
    }

    function changePage(event, button) {
        switch (button) {
            case "arrowLeft":
                if (currentPage != 0) {
                    setCurrentPage(currentPage - 1)
                    scrollTo()
                }
                break

            case "arrowRight":
                if (currentPage != pages - 1) {
                    setCurrentPage(currentPage + 1)
                    scrollTo()
                }
                break

            default:
                setCurrentPage(Number(event.target.value))
                scrollTo()
                break

        }
    }

    useEffect(() => {
        setPagesToShow(setArrayOfPages())
    }, [])

    useEffect(() => {
        setPagesToShow(setArrayOfPages())
    }, [currentPage, pages])

    return (
        <div className='pagination-container'>

            <button type='button' onClick={(e) => changePage(e, "arrowLeft")}><img src={leftArrow} ></img></button>

            {(pagesToShow.length != 0 ? pagesToShow : [0, 1, 2, "...", pages - 1]).map((item, index) => {

                if (item != "...") next = item + 1

                return (
                    <button
                        style={item === currentPage ? { color: "#53B38D" } : null}
                        key={index}
                        value={item != "..." ? item : next}
                        onClick={(e) => changePage(e)}>{item != "..." ? item + 1 : item}</button>
                )
            })}

            <button type='button' onClick={(e) => changePage(e, "arrowRight")}><img src={rightArrow} ></img></button>

        </div>
    )
}