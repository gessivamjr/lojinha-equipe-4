export default function limitItens(screenWidth, rows, columns) {

    let limit = 0;

    switch (true) {
        case (screenWidth < 539):
            limit = 5
            break

        default:
            limit = (rows != 3) ? 3 * columns : rows * columns

    }

    return limit
}