const mysql = require("../../database").pool;

exports.createProduct = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "INSERT INTO product (url, title, description, brand, color, category, subcategory, price) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
      [
        req.body.url,
        req.body.title,
        req.body.description,
        req.body.brand,
        req.body.color,
        req.body.category,
        req.body.subcategory,
        req.body.price,
      ],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(201).send({ message: "Product created" });
      }
    );
  });
};

exports.readProduct = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "SELECT * FROM product WHERE id = ?",
      [req.params.id],
      (error, result) => {
        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Product read", result: result });
      }
    );
  });
};

exports.readAllProducts = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query("SELECT * FROM product", (error, result) => {
      if (error) {
        return res.status(500).send({ error: error });
      }

      res.status(200).send({ message: "All products read", result: result });
    });
  });
};

exports.searchForProducts = (req, res) => {
  const paramsFormat = "%" + req.params.title + "%";
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "SELECT p.title, p.url, p.category, p.price FROM product as p WHERE p.title LIKE ?",
      [paramsFormat],
      (error, result) => {
        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Products founded", result: result });
      }
    );
  });
};

exports.updateProduct = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "UPDATE product SET url = ?, title = ?, description = ?, brand = ?, color = ?, category = ?, subcategory = ?, price = ? WHERE id = ?",
      [
        req.body.url,
        req.body.title,
        req.body.description,
        req.body.brand,
        req.body.color,
        req.body.category,
        req.body.subcategory,
        req.body.price,
        req.params.id,
      ],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Product updated" });
      }
    );
  });
};

exports.deleteProduct = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query("DELETE FROM product WHERE id = ?", [req.params.id], (error) => {
      conn.release();

      if (error) {
        return res.status(500).send({ error: error });
      }

      res.status(200).send({ message: "Product deleted" });
    });
  });
};
