import React from 'react';
import ExploreSection from '../../components/explore-section/ExploreSection';
import ProductsSection from '../../components/products-section/ProductsSection';
import ViewMore from '../../components/view-more/ViewMore';

export default function Home() {
  return (
    <div>
        <ExploreSection />

        <ViewMore />

        <ProductsSection title={"Produtos"} />

        
    </div>
  )
}

