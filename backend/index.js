const express = require("express");
const app = express();
const morgan = require("morgan");
const swaggerUi = require("swagger-ui-express");
const swaggerDocs = require("./swagger.json");

const cors = require("cors");
app.use(cors());

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan("dev"));
app.use("/api-docs/", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Header",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );

  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
    return res.status(200).send({});
  }

  next();
});

const productController = require("./product/controller/productController");
const cartController = require("./cart/controller/cartController");
const ordersController = require("./orders/controller/ordersController");
app.use("/product/", productController);
app.use("/cart/", cartController);
app.use("/orders/", ordersController);

app.use(() => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, res) => {
  res.status(error.status || 500);
  return res.send({
    error: { message: error.message },
  });
});

module.exports = app;
