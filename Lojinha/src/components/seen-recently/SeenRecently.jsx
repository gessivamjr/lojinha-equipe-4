import ProductCard from '../product-card/ProductCard'
import placeholder from '/images/placeholder.svg'
import './seen-recently.css'

export default function SeenRecently({ modalFunction }) {

    let product = {
        "id": 1,
        "imageUrl": placeholder,
        "price": "R$ 54.00",
        "category": "polo",
        "description": "camisa social polo original ",
        "quantity": 1,
        "quantityInStock": 10
    }

    return (
        <section className='seen-recently-container'>
            <h1>Visto recentemente</h1>
            <div className="seen-products">

                <ProductCard
                    imageUrl={product.imageUrl}
                    price={product.price}
                    description={product.description}
                    category={product.category}
                    id={product.id}
                    toggleModalFunction={modalFunction}
                />
                <div className="second-card">
                    <ProductCard
                        imageUrl={product.imageUrl}
                        price={product.price}
                        description={product.description}
                        category={product.category}
                        id={product.id}
                        toggleModalFunction={modalFunction}
                    />
                </div>

            </div>
        </section>

    )
}