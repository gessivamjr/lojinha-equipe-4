const express = require("express");
const router = express.Router();
const productService = require("../service/productService");

router.post("/create/", productService.createProduct);
router.get("/:id", productService.readProduct);
router.get("/", productService.readAllProducts);
router.get("/search/:title", productService.searchForProducts);
router.patch("/update/:id", productService.updateProduct);
router.delete("/delete/:id", productService.deleteProduct);

module.exports = router;
