import React from 'react';
import { useState } from 'react';
import './edit-product.css';

export default function EditProduct() {
    const [imgPreview, setImgPreview] = useState(null);
    const [error, setError] = useState(false);

    const handleImageChange = (e) => {
        setError(false);
        const selected = e.target.files[0];
        const ALLOWED_TYPES = ["image/png", "image/jpeg", "image/jpg", "image/svg+xml"];
        if (selected && ALLOWED_TYPES.includes(selected.type)) {
            let reader = new FileReader();

            reader.onloadend = () => {
                setImgPreview(reader.result);
            };
            reader.readAsDataURL(selected);
        } else {
            setError(true);
        }
    };

    return (
        <section className="register-body">
            <div className="form-title">
                <h1>Venha vender com a gente!</h1>
                <h2>Cadastre seu produto e venda na Lojinha</h2>
            </div>

            <div className="form-container">
                <div className="image-container">
                    {error && <p>File not supported</p>}

                    {imgPreview && (
                        <button className="delete-img" onClick={() => setImgPreview(null)}>Preview da Imagem</button>
                    )}

                    <div className="display-image" style={{ background: imgPreview ? `url("${imgPreview}") no-repeat center` : "#FAFAFA" }}>
                        {!imgPreview && (
                            <>
                                <label htmlFor="fileUpload" className="img-preview">
                                    <img src="images/fotos.svg" alt="" />
                                </label>

                                <input type="file" id="fileUpload" onChange={handleImageChange} style={{ display: "none" }} />
                            </>
                        )}
                    </div>

                </div>

                <form action="" className="checkout-form">
                    <div className="input-line">
                        <label>Url da Imagem</label>
                        <input type="text" />
                    </div>

                    <div className="input-line">
                        <label>Título</label>
                        <input type="text" />
                    </div>

                    <div className="input-line-2">
                        <div className="double-title">
                            <label className="title-1">Descrição do produto</label>
                            <label className="title-2">(max. 300 caracteres)</label>
                        </div>

                        <textarea className="product-description" maxLength="300"></textarea>

                        <div className="hidden-title">
                            <label className="title-hidden">(max. 300 caracteres)</label>
                        </div>
                    </div>

                    <div className="input-container">
                        <div className="input-line brand">
                            <label>Marca</label>
                            <input type="text" />
                        </div>

                        <div className="input-line">
                            <label>Cor</label>
                            <input type="text" />
                        </div>
                    </div>

                    <div className="select-container">
                        <div className="input-line category">
                            <label>Categoria</label>
                            <select className="arrow" name="" id=""></select>
                        </div>

                        <div className="input-line">
                            <label>Subcategoria</label>
                            <select className="arrow" name="" id=""></select>
                        </div>
                    </div>

                    <div className="input-line half">
                        <label>Preço</label>
                        <input type="text" />
                    </div>

                    <div className="register-btn">
                        <input type="button" value="Editar produto" />
                    </div>
                </form>
            </div>
        </section>
    )
}
