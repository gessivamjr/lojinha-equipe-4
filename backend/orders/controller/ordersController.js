const express = require("express");
const router = express.Router();
const ordersService = require("../service/ordersService");

router.post("/create/:id_product", ordersService.createOrder);
router.get("/:id_order", ordersService.readOrder);
router.get("/", ordersService.readAllOrders);
router.patch("/update/:id_product", ordersService.updateOrder);
router.delete("/delete/:id_order", ordersService.deleteOrder);

module.exports = router;
