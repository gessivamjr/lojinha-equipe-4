import { useState } from 'react'
import { useEffect } from 'react'
import ModalProductVariations from '../product-variations/ModalProductVariations.jsx'
import mockData from '../../utils/mockData'
import closeIcon from '/icons/close.svg'
import addIcon from '/icons/circle-add.svg'
import subIcon from '/icons/circle-sub.svg'
import editIcon from '/icons/edit.svg'
import deleteIcon from '/icons/trash.svg'
import './modal.css'
import './modal-responsive.css'

import { Link } from 'react-router-dom';

export default function Modal({ isShowing, setIsShowing, id }) {

    const [item, setItem] = useState({})
    const [quantity, setQuantity] = useState(1)


    //closes modal if user clicks outside of the modal itself
    function handleClick(event) {
        event.target.className == 'modal-background' || event.target.className == 'close-button' ? setIsShowing(false) : null
    }

    function changeQuantity(isAdding) {

        if (isAdding == true) {
            quantity == item.quantityInStock ? null : setQuantity(quantity + 1)
        }
        else {
            quantity == 0 ? null : setQuantity(quantity - 1)
        }
    }


    //just a placeholder to find data about the item until integration w/ backend
    useEffect(() => {
        const item = mockData()
        setItem(item[id])
        setQuantity(1)
    }, [id])

    useEffect(() => {
        item.quantity = quantity
    }, [quantity])

    // useEffect(() => {
    //     console.log(item)
    // })


    return (
        <section>
            {isShowing == true && item ?
                <div onClick={(e) => handleClick(e)} className="modal-background">
                    <div className="modal-container">

                        <div className="modal-content">
                            {/* {item != {} ? <h1>{item.description}</h1> : <h1>Carregando...</h1>} */}
                            <header className="modal-header">
                                <h4>{item.description}</h4>
                            </header>
                            <button id='close-modal-button' className='close-button' onClick={(e) => handleClick(e)} type='button'>
                                <img className='close-button' src={closeIcon} alt="ícone de X para fechar o modal" />
                            </button>

                            <div className="product-info">
                                <div className="column-content">
                                    <div className="first-column">
                                        <div className="modal-product-description">
                                            <h2 className='info-title'>Descrição</h2>
                                            <p className='product-content'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis asperiores assumenda ex vero veniam? Nesciunt, assumenda ipsam dolores accusamus a suscipit consequuntur nostrum facere ut. Officiis recusandae totam eius nesciunt.</p>
                                        </div>
                                        <div className="product-vendor">
                                            <h2 className='info-title'>Vendedor</h2>
                                            <p className='product-content'>Lorem ipsum dolor</p>
                                        </div>

                                        <div className="product-brand">
                                            <h2 className='info-title'>Marca</h2>
                                            <p className='product-content'>Lorem ipsum dolor</p>
                                        </div>

                                        <div className="product-color">
                                            <h2 className='info-title'>Cor</h2>
                                            <p className='product-content'>Lorem ipsum dolor</p>
                                        </div>

                                        <div className="modal-product-price">
                                            <h3>{item.price}</h3>
                                        </div>

                                        <div className="modal-actions">
                                            <div className="admin-actions">
                                                {/* <button className="edit-action">
                                                    <img src={editIcon} />
                                                    <span id="modal-edit-button" className='action'>
                                                        Editar 
                                                    </span>    
                                                </button> */}

                                                <button className="edit-action">
                                                    <img src={editIcon} />
                                                    <span >
                                                        <Link id="modal-edit-button" className='action' to="/edit">Editar</Link>
                                                    </span>
                                                </button>

                                                <button className="delete-action">
                                                    <img src={deleteIcon} />
                                                    <span id="modal-delete-button" className='action'>Deletar</span>
                                                </button>
                                            </div>
                                            <div className="add-to-cart-btn">
                                                <button>Adicionar ao carrinho</button>
                                            </div>
                                        </div>


                                    </div>
                                    <div className='second-column'>
                                        <div className="second-column-content">
                                            <img className="modal-product-image" src={item.imageUrl} alt={"imagem de " + item.description} />
                                            <ModalProductVariations variations={["P", "M", "G"]} />
                                            <div className="quantity-container">
                                                <button onClick={(e) => changeQuantity(false)}><img src={subIcon} alt="botão com sinal de subtração" /></button>
                                                <p> {quantity} </p>
                                                <button onClick={(e) => changeQuantity(true)}><img src={addIcon} alt="botão com sinal de adição" /></button>
                                            </div>
                                        </div>
                                        <div className="client-actions">
                                            <button>Adicionar ao carrinho</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="row-content">

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                : null}
        </section >


    )
}