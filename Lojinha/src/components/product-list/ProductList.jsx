import { useState, useEffect } from 'react'
import './product-list.css'
import ProductCard from '../product-card/ProductCard'
import PaginationComponent from '../pagination-component/PaginationComponent'
import debounce from '../../utils/debounce'
import retriveDimensions from '../../utils/retriveDimensions'
import mockData from '../../utils/mockData'
import limitItens from '../../utils/limitItens'
import Modal from '../modal/Modal';
import Loader from '../loader/Loader'


export default function ProductList({ scrollTo, handleModal }) {


    //catalog pagination states
    const [itens, setItens] = useState([])
    const [itensPerPage, setItensPerPage] = useState(16)
    const [currentPage, setCurrentPage] = useState(0)


    //state of window's dimensions
    const [dimensions, setDimensions] = useState({
        height: window.innerHeight,
        width: window.innerWidth
    })


    //variables to define number of pages and itens on each page
    const pages = Math.ceil(itens.length / itensPerPage)
    const startIndex = currentPage * itensPerPage
    const endIndex = startIndex + itensPerPage
    let currentItens = itens.slice(startIndex, endIndex)


    function defineItensPerPage(screenWidth) {

        let { rows, columns } = retriveDimensions(".product-list");
        setItensPerPage(limitItens(screenWidth, rows, columns))
    }

    async function getProducts() {

        const response = await fetch('http://localhost:3001/product/');
        const data = await response.json()

        if (!data.error)
            setItens(data.result)
    }

    //retrives product list and define number of itens per page
    useEffect(() => {

        getProducts();
        defineItensPerPage(dimensions.width)

    }, [])


    //return to the first page and to the top of catalog 
    useEffect(() => {
        setCurrentPage(0)
        scrollTo()
    }, [itensPerPage])


    //handles window resize
    useEffect(() => {

        //handle window resize 10x per second
        const debouncedHandleResize = debounce(function handleResize() {

            defineItensPerPage(window.innerWidth)

            setDimensions({
                height: window.innerHeight,
                width: window.innerWidth
            })
        }, 100)

        //calls debouncedHandleResize on window resize
        window.addEventListener('resize', debouncedHandleResize)


        //removes event listener
        return () => {
            window.removeEventListener('resize', debouncedHandleResize)

        }
    })


    return (
        <div className="product-list-container">
            <section className="product-list">
                {currentItens.length != 0 ? currentItens.map((item, index) => {
                    return (
                        <div key={index} className="card">
                            <ProductCard
                                key={index}
                                imageUrl={item.url}
                                price={item.price}
                                title={item.title}
                                category={item.category}
                                id={item.id}
                                toggleModalFunction={handleModal}
                            />
                        </div>
                    )
                }) : <div className='loader-container'><Loader /></div>}
            </section>
            {currentItens.length != 0 ? <PaginationComponent setCurrentPage={setCurrentPage} pages={pages} currentPage={currentPage} scrollTo={scrollTo} /> : null}

        </div>


    )
}