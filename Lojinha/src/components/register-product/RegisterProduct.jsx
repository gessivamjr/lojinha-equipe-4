import React from 'react';
import { useState } from 'react';
import './register-product.css';

export default function RegisterProduct() {
    const [imgPreview, setImgPreview] = useState(null);
    const [error, setError] = useState(false);

    const handleImageChange = (e) => {
        setError(false);
        const selected = e.target.files[0];
        const ALLOWED_TYPES = ["image/png", "image/jpeg", "image/jpg", "image/svg+xml"];
        if (selected && ALLOWED_TYPES.includes(selected.type)) {
            let reader = new FileReader();

            reader.onloadend = () => {
                setImgPreview(reader.result);
            };
            reader.readAsDataURL(selected);
        } else {
            setError(true);
        }
    };

    const [url, setURL] = useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [brand, setBrand] = useState('');
    const [color, setColor] = useState('');
    const [category, setCategory] = useState('');
    const [subcategory, setSubcategory] = useState('');
    const [price, setPrice] = useState(0);

    console.log(title);

    // const product = document.querySelectorAll(".product-input")
    async function createProduct() {
        await fetch("http://localhost:3001/product/create", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                url: url,
                title: title,
                description: description,
                brand: brand,
                color: color,
                category: category,
                subcategory: subcategory,
                price: parseInt(price)
            }),
        })
            // .then()
            .catch((err) => console.log(err));
    }

    return (
        <section className="register-body">
            <div className="form-title">
                <h1>Venha vender com a gente!</h1>
                <h2>Cadastre seu produto e venda na Lojinha</h2>
            </div>

            <div className="form-container">
                <div className="image-container">
                    {error && <p>File not supported</p>}

                    {imgPreview && (
                        <button className="delete-img" onClick={() => setImgPreview(null)}>Preview da Imagem</button>
                    )}

                    <div className="display-image" style={{ background: imgPreview ? `url("${imgPreview}") no-repeat center` : "#FAFAFA" }}>
                        {!imgPreview && (
                            <>
                                <label htmlFor="fileUpload" className="img-preview">
                                    <img src="images/fotos.svg" alt="" />
                                </label>

                                <input type="file" id="fileUpload" onChange={handleImageChange} style={{ display: "none" }} />
                            </>
                        )}
                    </div>

                </div>

                <form action="" className="checkout-form">
                    <div className="input-line">
                        <label>Url da Imagem</label>
                        <input
                            type="text"

                            onChange={(event) => {
                                setURL(event.target.value);
                            }}
                        />
                    </div>

                    <div className="input-line">
                        <label>Título</label>
                        <input
                            type="text"

                            onChange={(event) => {
                                setTitle(event.target.value);
                            }}
                        />
                    </div>

                    <div className="input-line-2">
                        <div className="double-title">
                            <label className="title-1">Descrição do produto</label>
                            <label className="title-2">(max. 300 caracteres)</label>
                        </div>

                        <textarea
                            className="product-description" maxLength="300"

                            onChange={(event) => {
                                setDescription(event.target.value);
                            }}
                        >

                        </textarea>

                        <div className="hidden-title">
                            <label className="title-hidden">(max. 300 caracteres)</label>
                        </div>
                    </div>

                    <div className="input-container">
                        <div className="input-line brand">
                            <label>Marca</label>
                            <input
                                type="text"

                                onChange={(event) => {
                                    setBrand(event.target.value);
                                }}
                            />
                        </div>

                        <div className="input-line">
                            <label>Cor</label>
                            <input
                                type="text"

                                onChange={(event) => {
                                    setColor(event.target.value);
                                }}
                            />
                        </div>
                    </div>

                    <div className="select-container">
                        <div className="input-line category">
                            <label>Categoria</label>
                            <select

                                className="arrow" name="category" id=""
                                onChange={(event) => {
                                    setCategory(event.target.value);
                                }}
                            >
                                <option value="1">Camisa</option>
                            </select>
                        </div>

                        <div className="input-line">
                            <label>Subcategoria</label>
                            <select
                                className="arrow" name="" id=""
                                onChange={(event) => {
                                    setSubcategory(event.target.value);
                                }}
                            >
                                <option value="2">Bermuda</option>
                            </select>
                        </div>
                    </div>

                    <div className="input-line half">
                        <label>Preço</label>
                        <input
                            type="number"
                            onChange={(event) => {
                                setPrice(event.target.value);
                            }}
                        />
                    </div>

                    <div className="register-btn">
                        <input onClick={createProduct} type="button" value="Cadastrar produto" />
                    </div>
                </form>
            </div>
        </section>
    )
}
