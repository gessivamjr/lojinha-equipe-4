import React from 'react';
import './header.css';
import { Link } from 'react-router-dom';

export default function Header() {
    return (
        <header className="main-header" id="home">
            <input type="checkbox" id="check"/>

            <div className="header-top-bar">
                <div className="logo">
                    <img src="images/Logo.svg" alt="ícone de uma sacola de compras verde" />

                    <Link to="/">Lojinha</Link>
                </div>
            </div>

            <div className="search-box">
                <div className="search-input">
                    <input type="text" placeholder="Pesquise por um produto" />
                    <img src="images/Search-icon.svg" alt="ícone de uma lupa de pesquisa" />
                </div>
            </div>

            <nav className="nav">
                <ol>
                    <li>
                        <Link to="/">Início</Link>
                    </li>
                    <li>
                        <a  href="#products"> Produtos</a>
                        
                    </li>

                    <li>
                        <Link to="/announce">Anunciar</Link>
                    </li>

                    <li>
                        <button >
                            <Link className="btn" to="/shop">Carrinho</Link>
                        </button>
                    </li>
                </ol>

                <label className="hamburguer-menu" for="check">
                    <span className="fa fa-bars" id="bars"></span>
                    <span className="fa fa-times" id="times"></span>
                </label>
            </nav>
        </header>
    )
}
