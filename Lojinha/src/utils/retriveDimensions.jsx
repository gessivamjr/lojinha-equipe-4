export default function retriveDimensions(selector) {

    const gridComputedStyle = window.getComputedStyle(document.querySelector(selector));

    // get number of grid rows
    const gridRowCount = gridComputedStyle.getPropertyValue("grid-template-rows").split(" ").length;

    // get number of grid columns
    const gridColumnCount = gridComputedStyle.getPropertyValue("grid-template-columns").split(" ").length;

    return {
        rows: gridRowCount,
        columns: gridColumnCount
    }

}