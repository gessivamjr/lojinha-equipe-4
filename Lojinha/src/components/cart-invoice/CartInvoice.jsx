import { useEffect, useState } from 'react'
import './cart-invoice.css'

export default function CartInvoice({ subtotal, qntItens }) {

    const [total, setTotal] = useState(subtotal)
    const [shipmentPrice, setshipmentPrice] = useState(0)

    useEffect(() => {
        setTotal(subtotal)
    }, [subtotal])

    return (
        <div className='cart-invoice-container'>
            <div className="cart-invoice-content">
                <h1>Resumo do Pedido</h1>
                <hr />
                <div className="cart-invoice">
                    <div className="invoice-subtotal">
                        <p>Subtotal {qntItens} ({qntItens == 1 ? "item" : "itens"}) </p>
                        <p className='invoice-price'>R$ {subtotal}</p>
                    </div>
                    <div className="invoice-shipment">
                        <p>Entrega </p>
                        <p className='invoice-price'>R$ {shipmentPrice}</p>
                    </div>
                </div>
                <hr />
                <div className="invoice-total">
                    <p>Total</p>
                    <p className='invoice-total-price'>R$ {total}</p>
                </div>
            </div>
            <div className="invoice-button">
                <button type='button'>Finalizar compra</button>
            </div>
        </div>
    )
}