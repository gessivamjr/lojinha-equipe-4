const express = require("express");
const router = express.Router();
const cartService = require("../service/cartService");

router.post("/create/:id_product", cartService.createCart);
router.get("/:id_cart", cartService.readCart);
router.patch("/increase/:id_cart", cartService.updateIncreaseCart);
router.patch("/decrease/:id_cart", cartService.updateDecreaseCart);
router.delete("/delete/:id_cart", cartService.deleteCart);

module.exports = router;
