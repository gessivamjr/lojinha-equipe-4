import arrow from "/icons/right-arrow.svg"
import "./main-product.css"

export default function MainProduct({ imageUrl, name }) {
    return (
        <div className="main-product">
            <img className="main-product-image" src={imageUrl}></img>
            <div className="main-product-callout">
                <h4>{name}</h4>
                <a href="#">Ver Mais<img src={arrow}></img> </a>
            </div>
            <div className="shape"></div>
        </div>
    )
}