import MainProduct from "../main-product-card/MainProduct"
import shirt from "/images/camisa.svg"
import purse from "/images/bolsa.svg"
import sneakers from "/images/tenis.svg"
import "./view-more.css"

export default function ViewMore() {
    return (
        <section className="main-products-container">
            <MainProduct imageUrl={shirt} name="Camisas" />
            <MainProduct imageUrl={sneakers} name="Tênis" />
            <MainProduct imageUrl={purse} name="Bolsas" />
        </section>
    )
}