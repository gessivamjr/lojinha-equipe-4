import { useRef, useState } from 'react'
import Modal from '../modal/Modal'
import ProductList from '../product-list/ProductList'
import './products-section.css'

export default function ProductsSection({ title }) {
    const titleRef = useRef()

    const [showModal, setShowModal] = useState(false)
    const [id, setId] = useState(0)

    function handleClick(event, id) {
        setId(id)
        showModal == false ? setShowModal(true) : setShowModal(false)
    }

    function handleBackClick() {
        titleRef.current.scrollIntoView({ behavior: 'smooth' })
    }

    return (
        <section className="products-section">
            <div className="products-section-container" id='products'>
                <h1 ref={titleRef}>{title}</h1>
                <div className="list">
                    <ProductList
                        scrollTo={handleBackClick}
                        handleModal={handleClick}
                    />
                </div>
            </div>
            <Modal isShowing={showModal} setIsShowing={setShowModal} id={id} />
        </section>
    )
}

