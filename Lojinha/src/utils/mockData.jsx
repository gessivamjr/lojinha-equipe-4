import placeholder from '/images/placeholder.svg'

export default function mockData(quantity) {

    let data = [];

    Array.from({ length: (quantity ? quantity : 100) }, (item, index) => {
        data.push({
            "id": index,
            "imageUrl": placeholder,
            "price": "54.00",
            "category": "polo",
            "description": "camisa social polo original " + index,
            "quantity": 1,
            "quantityInStock": 10,
            "seller": "Joaninha",
            "color": "Azul",
            "size": "M"
        })
    })

    return data

}