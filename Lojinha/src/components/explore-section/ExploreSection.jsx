import { useEffect, useState } from "react";
import illustration from '/images/compras.svg'
import arrowDown from '/icons/chevron-down.svg'
import './explore-section.css'

export default function ExploreSection() {

    return (
        <section className="explore-section-container">
            <div className="explore-section-content">
                <div className="explore-section-details">
                    <div className="details-callout">
                        <h1>Explore nossa nova coleção</h1>
                        <p>Aproveite as ofertas e encontre o look ideal para aproveitar o seu São João na Lojinha.</p>
                    </div>
                    <a href="#">
                        <span>Ver descontos</span>
                        <img src={arrowDown} alt="ícone de seta apontando para baixo"></img>
                    </a>
                </div>
                <img id="explore-section-img" src={illustration} alt="ilustração de sacolas de compras"></img>
            </div>
        </section>
    );

}