import './modal-product-variations.css'

export default function ModalProductVariations({ variations }) {
    return (
        <div className="modal-product-variations-container">
            {variations.map((item, index) => {
                return (
                    <button key={index} type='button' value={item} className='modal-product-variations'>{item}</button>
                )
            })}
        </div>
    )
}