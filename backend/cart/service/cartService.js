const mysql = require("../../database").pool;

exports.createCart = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "INSERT INTO cart (id_product, qnt_product) VALUES (?, 1)",
      [req.params.id_product],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(201).send({ message: "Cart created" });
      }
    );
  });
};

exports.readCart = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "SELECT p.title, p.url, p.category, p.price FROM product as p INNER JOIN cart as c ON c.id_product = p.id WHERE c.id_cart = ?",
      [req.params.id_cart],
      (error, result) => {
        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Cart read", result: result });
      }
    );
  });
};

exports.updateIncreaseCart = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "UPDATE cart SET qnt_product = qnt_product + 1 WHERE id_cart = ?",
      [req.params.id_cart],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Quantity of products increased" });
      }
    );
  });
};

exports.updateDecreaseCart = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "UPDATE cart SET qnt_product = qnt_product - 1 WHERE id_cart = ?",
      [req.params.id_cart],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Quantity of products decreased" });
      }
    );
  });
};

exports.deleteCart = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "DELETE FROM cart WHERE id_cart = ?",
      [req.params.id_cart],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Cart deleted" });
      }
    );
  });
};
