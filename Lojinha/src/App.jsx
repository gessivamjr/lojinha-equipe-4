import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Footer from './components/footer/Footer'
import Header from './components/header/Header'
import Home from './pages/home/Home'
import CartPage from './pages/cart-page/CartPage'
import Announce from './pages/register-product/Announce'
import Edit from './pages/edit-product/Edit'

function App() {
  return (
    <div>
      <Router>
        <Header />
        <Routes>
          <Route path='/' exact element={<Home />} />
          <Route path='/announce' element={<Announce />} />
          <Route path='/edit' element={<Edit />} />
          <Route path='/shop' element={<CartPage />} />
        </Routes>
        <Footer />
      </Router>
    </div>
  )
}

export default App

