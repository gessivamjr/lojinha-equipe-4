import { useState, useEffect } from 'react'
import mockData from '../../utils/mockData'
import ProductTile from '../product-tile/ProductTile'
import EmptyCart from "../empty-cart/EmptyCart"
import './cart-list.css'

export default function CartList({ setSubtotal }) {

    const [itens, setItens] = useState([])
    let flag = 1;

    function deleteProductOnCart(id) {
        if (confirm("Retirar do carrinho?")) {
            setItens(itens.filter(e => e.id != id))
        }

        //TODO: remove item from cart table
    }

    useEffect(() => {

        if (flag == 1) {
            setItens(mockData(3))
            flag--;
        }

    }, [])

    useEffect(() => {

        let total = 0

        itens.forEach(item => {
            total += Number(item.price)
        })

        setSubtotal(total);

    }, [itens])

    return (
        <table className='cart-list-container'>
            {itens.length > 0 ? itens.map((item, index) => {
                return (
                    <ProductTile
                        key={index}
                        product={item}
                        deleteProduct={deleteProductOnCart} />
                )
            }) : null}

        </table>
    )
}