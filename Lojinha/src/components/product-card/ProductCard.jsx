import { useState } from 'react';
import Modal from '../modal/Modal';
import './product-card.css'

export default function ProductCard({ imageUrl, price, title, category, id, toggleModalFunction }) {

    let arraySizes = ["P", "M", "G"];

    return (
        <a onClick={e => toggleModalFunction(e, id)} className="card-container">
            <div className="product-image">
                <img src={imageUrl} />
                <div className="product-variations-container">
                    {arraySizes.map((item, index) => {
                        return (
                            <button key={index} type='button' value={item} className='product-variations'>{item}</button>
                        )
                    })}
                </div>
            </div>

            <div className="product-details">
                <p className='product-price'>{price}</p>
                <p className='card-product-description'>{title}</p>
                <p className='product-category'>{category}</p>
            </div>
        </a>
    )
}