const mysql = require("../../database").pool;

exports.createOrder = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "INSERT INTO orders (id_product, qnt_products, total_price) VALUES (?, ?, ?)",
      [req.params.id_product, req.body.qnt_products, req.body.total_price],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        return res.status(201).send({ message: "Order created" });
      }
    );
  });
};

exports.readOrder = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "SELECT * FROM orders WHERE id_order = ?",
      [req.params.id_order],
      (error, result) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Order read", result: result });
      }
    );
  });
};

exports.readAllOrders = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query("SELECT * FROM orders", (error, result) => {
      conn.release();

      if (error) {
        return res.status(500).send({ error: error });
      }

      res.status(200).send({ message: "All orders read", result: result });
    });
  });
};

exports.updateOrder = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "UPDATE orders SET id_product = ?, qnt_products = ?, total_price = ?",
      [req.params.id_product, req.body.qnt_products, req.body.total_price],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Order updated" });
      }
    );
  });
};

exports.deleteOrder = (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      "DELETE FROM orders WHERE id_order = ?",
      [req.params.id_order],
      (error) => {
        conn.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        res.status(200).send({ message: "Order deleted" });
      }
    );
  });
};
